package edu_bot.main_class;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import javax.sql.DataSource;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@ComponentScan("edu_bot")
@Configuration
public class AppConfig
{
    @Bean
    public JdbcTemplate jdbcTemplate()
    {
        return new JdbcTemplate(dataSource());
    }


   @Bean
   public DataSource dataSource() {
       SimpleDriverDataSource dataSource = new  SimpleDriverDataSource();
       //dataSource.setDriverClass(org.postgresql.Driver.class);
       /*String dbName = "";
       String userName = "";
       String password = "";
       String hostname = "";
       String port = "";
       String jdbcUrl = "jdbc:postgresql://" + hostname + ":" + port + "/" + dbName + "?user=" + userName + "&password=" + password;
       dataSource.setUrl(jdbcUrl);*/
       dataSource.setDriverClass(org.h2.Driver.class);
       dataSource.setUsername("sa");
       dataSource.setPassword("");
       dataSource.setUrl("jdbc:h2:file:./H2/H2_DateBase;init=runscript from './schema.sql';IFEXISTS=false;DB_CLOSE_ON_EXIT=FALSE");
       //dataSource.setUrl("jdbc:h2:file:./H2/H2_DateBase;IFEXISTS=false;DB_CLOSE_ON_EXIT=FALSE;INIT=CREATE SCHEMA IF NOT EXISTS schema;");
       return dataSource;
   }
}
