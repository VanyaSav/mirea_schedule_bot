package edu_bot.main_class;

import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;

    public class Config
    {

        /** Путь к bot.properties */
        private static final  String CONFIGURATION_BOT_FILE = "/config/bot/bot.properties";

        /** =================# Блок настройки значений #================= */

        /** Имя бота */
        public static String BOT_NAME;
        /** Токен бота */
        public static String BOT_TOKEN;

        /** Метод загрузки конфига */
        public static void load()
        {
            Properties botSettings = new Properties();
            try
            {
                InputStream is = Main.class.getResourceAsStream(CONFIGURATION_BOT_FILE);
                botSettings.load(is);
                Main._Log.info("Конфиг загружается");
                is.close();
                Main._Log.info("Конфиг бота успешно загружен");
            }
            catch (Exception e)
            {
                Main._Log.fatal("Ошибка загрузки конфига Бота", e);
            }

            BOT_NAME = botSettings.getProperty("BotName", "HappyEduBot");
            BOT_TOKEN = botSettings.getProperty("BotToken", "505330739:AAF8EvKA7TP1lvRf3ZrCITcHX0LWlGNU0bg");
        }
    }
