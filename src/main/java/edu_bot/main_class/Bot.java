package edu_bot.main_class;

import edu_bot.additional_class.Emoji;
import edu_bot.additional_class.Sticker;
import edu_bot.db_class.dao.*;
import edu_bot.db_class.model.ClassTime;
import edu_bot.db_class.model.Schedule;
import edu_bot.db_class.model.Teacher;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.ActionType;
import org.telegram.telegrambots.api.methods.send.*;
import org.telegram.telegrambots.api.objects.*;
import org.telegram.telegrambots.api.objects.replykeyboard.ForceReplyKeyboard;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
    public class Bot extends TelegramLongPollingBot
    {

        private final UserDao userDao;
        private final ClassroomDao classroomDao;
        private final ScheduleDao scheduleDao;
        private final GroupDao groupDao;
        private final TeacherDao teacherDao;
        private final ClassTimeDao classTimeDao;
        private final SubjectDao subjectDao;
        private final EducationDateDao educationDateDao;
        private final JdbcTemplate jdbcTemplate;

        public Bot(UserDao userDao, ClassroomDao classroomDao, ScheduleDao scheduleDao, GroupDao groupDao,
                   TeacherDao teacherDao, ClassTimeDao classTime, SubjectDao subjectDao, EducationDateDao educationDateDao,
                   JdbcTemplate jdbcTemplate)
        {
            this.userDao = userDao;
            this.classroomDao = classroomDao;
            this.scheduleDao = scheduleDao;
            this.groupDao = groupDao;
            this.teacherDao = teacherDao;
            this.classTimeDao = classTime;
            this.subjectDao = subjectDao;
            this.educationDateDao = educationDateDao;
            this.jdbcTemplate = jdbcTemplate;
        }

        /** Получение имени бота */
        @Override
        public String getBotUsername() {
            return Config.BOT_NAME;
        }

        /** Получение конфига бота */
        @Override
        public String getBotToken() {
            return Config.BOT_TOKEN;
        }

        /** Получение новых сообщений в диалогах */
        @Override
        public void onUpdateReceived(Update update) {
            Message msg = update.getMessage();
            Message msg_update = update.getEditedMessage();

            /** Получение сообщений от пользователя */
            if (update.hasMessage() && msg.hasText())
            {
                checkUpdate(update, msg);
            }
            else if (update.hasEditedMessage() && msg_update.hasText())
            {
                checkUpdate(update, msg_update);
            }

            /** Метод получения кода отправленных фото */
            /**else if (e.hasMessage() && e.getMessage().getPhoto() != null)
             {
             List<PhotoSize> photos = e.getMessage().getPhoto();
             String f_id = photos.stream()
                                .sorted(Comparator.comparing(PhotoSize::getFileSize).reversed())
                                .findFirst()
                                .orElse(null).getFileId();
             sendMsg(msg, f_id); // Call method to send the message
             }*/
        }

        /** Получение финального сообщения в зависимости от полученного */
        private Message unionMessage(Update update)
        {
            String txt;
            Message msg = update.getMessage();
            Message msg_update = update.getEditedMessage();
            Message msg_final = null;

            if (update.hasMessage() && update.getMessage().hasText())
            {
                txt = msg.getText();
                Main._Log.info("Пользователь " + msg.getFrom().getUserName() + " написал '" + txt + "'\n");
                msg_final = msg;
            }
            else if (update.hasEditedMessage() && msg_update.hasText())
            {
                txt = msg_update.getText();
                Main._Log.info("Пользователь " + msg_update.getFrom().getUserName() + " изменил сообщение на '" + txt + "'\n");
                msg_final = msg_update;
            }
            return msg_final;
        }

        /** Проверка на регистрацию пользователя */
        private void checkUpdate (Update update, Message msg)
        {
            String txt = msg.getText();
            if (!msg.isReply() && (userDao.getUsersById(msg.getChatId()).size() != 0 || (txt.equals("/start")
                    || txt.equals("/start@MireaSchedule_Bot") || txt.equals("/start@HappyEduBot"))))
            {
                Message msg_final = unionMessage(update);
                checkMessage(msg_final);
            }
            else if (msg.isReply() && (userDao.getUsersById(msg.getChatId()).size() != 0
                    || (msg.getReplyToMessage().getText().contains(Emoji.Card_Index + "Регистрация:\n"))))
            {
                Message msg_final = unionMessage(update);
                checkReplyMessage(msg_final);
            }
            else if (userDao.getUsersById(msg.getChatId()).size() == 0 && !(txt.equals("/start")
                    || txt.equals("/start@MireaSchedule_Bot") || txt.equals("/start@HappyEduBot")))
            {
                sendMsg(msg, "К сожалению вы не зарегистрированы. Чтобы зарегистрироваться введите /start", true);
            }
        }

        /** Входит ли текущая дата в семестр */
        private Boolean isSemester(Calendar c)
        {
            Boolean isSemester = false;

            Date currentDate = c.getTime();

            Date semesterStartDate = educationDateDao.getEducationDate().getSemesterStartDate();
            Date testSessionStartDate = educationDateDao.getEducationDate().getTestSessionStartDate();

            if (testSessionStartDate != null && semesterStartDate != null)
            {
                Calendar c1 = Calendar.getInstance();
                c1.setTime(testSessionStartDate);
                c1.add(Calendar.DATE, -1);
                Date semesterStopDate = c1.getTime();

                if ((currentDate.after(semesterStartDate) || currentDate.equals(semesterStartDate)) &&
                        (currentDate.before(semesterStopDate) || currentDate.equals(semesterStopDate)))
                    isSemester = true;
            }
            else if (semesterStartDate != null && testSessionStartDate == null)
            {
                if (currentDate.after(semesterStartDate) || currentDate.equals(semesterStartDate))
                    isSemester = true;
            }

            return isSemester;
        }

        /** Входит ли текущая дата в зачетную сессию */
        private Boolean isTestSession(Calendar c)
        {
            Boolean isTestSession = false;

            Date testSessionStartDate = educationDateDao.getEducationDate().getTestSessionStartDate();
            Date examSessionStartDate = educationDateDao.getEducationDate().getExamSessionStartDate();

            Date currentDate = c.getTime();

            if (testSessionStartDate != null && examSessionStartDate != null)
            {
                Calendar c1 = Calendar.getInstance();
                c1.setTime(examSessionStartDate);
                c1.add(Calendar.DATE, -1);
                Date testSessionStopDate = c1.getTime();

                if ((currentDate.after(testSessionStartDate) || currentDate.equals(testSessionStartDate)) &&
                        (currentDate.before(testSessionStopDate) || currentDate.equals(testSessionStopDate)))
                    isTestSession = true;
            }
            else if (testSessionStartDate != null && examSessionStartDate == null)
            {
                if (currentDate.after(testSessionStartDate) || currentDate.equals(testSessionStartDate))
                    isTestSession = true;
            }
            return isTestSession;
        }

        /** Входит ли текущая дата в экзаменационную сессию */
        private Boolean isExamSession(Calendar c)
        {
            Boolean isExamSession = false;

            Date currentDate = c.getTime();

            Date examSessionStartDate = educationDateDao.getEducationDate().getExamSessionStartDate();
            Date examSessionStopDate = educationDateDao.getEducationDate().getExamSessionStopDate();

            if (examSessionStartDate != null && examSessionStopDate != null)
            {
                if ((currentDate.after(examSessionStartDate) || currentDate.equals(examSessionStartDate)) &&
                        (currentDate.before(examSessionStopDate) || currentDate.equals(examSessionStopDate)))
                    isExamSession = true;
            }
            else if (examSessionStartDate != null && examSessionStopDate == null)
            {
                if (currentDate.after(examSessionStartDate) || currentDate.equals(examSessionStartDate))
                    isExamSession = true;
            }

            return isExamSession;
        }

        /** Вычисляет текущую неделю */
        private Integer getCurrentWeek(Calendar c)
        {
            Integer currentWeek = null;

            if (isSemester(c))
            {
                Date semesterStartDate = educationDateDao.getEducationDate().getSemesterStartDate();

                Integer week = Integer.parseInt(new SimpleDateFormat( "ww", new Locale("ru")).format(semesterStartDate));
                currentWeek = Math.abs(c.get(Calendar.WEEK_OF_YEAR) - week) + 1;
            }
            else if (isTestSession(c))
            {
                Date semesterStartDate = educationDateDao.getEducationDate().getTestSessionStartDate();

                Integer week = Integer.parseInt(new SimpleDateFormat( "ww", new Locale("ru")).format(semesterStartDate));
                currentWeek = Math.abs(c.get(Calendar.WEEK_OF_YEAR) - week) + 1;
            }
            else if (isExamSession(c))
            {
                Date semesterStartDate = educationDateDao.getEducationDate().getExamSessionStartDate();

                Integer week = Integer.parseInt(new SimpleDateFormat( "ww", new Locale("ru")).format(semesterStartDate));
                currentWeek = Math.abs(c.get(Calendar.WEEK_OF_YEAR) - week) + 1;
            }

            return currentWeek;
        }

        /** Выполнение необходимого условия в зависимости от полученного сообщения */
        private void checkMessage(Message msg_final)
        {

            String txt = msg_final.getText();

            if (txt.equals("/start") || txt.equals("/start@MireaSchedule_Bot") || txt.equals("/start@HappyEduBot"))
            {
                sendChatActionTyping(msg_final);
                if (userDao.getUsersById(msg_final.getChatId()).size() == 0)
                {

                    sendMsgReply(msg_final, Emoji.Card_Index + "Регистрация:\n*Приветствую!*\n" +
                            "Хотите ли вы сразу добавить группу, расписание которой будете получать в дальнейшем?\n" +
                            "Если да, то введите группу в формате XXXX-YY-YY (X - буквы, Y - цифры),\n" +
                            "если нет, то введите в ответ слово \"Нет\"");
                }
                else
                {
                    sendMsg(msg_final, "Вы уже зарегистрированы!", true);
                    sendStartKeyboard(msg_final, false);
                }

            }
            else if (txt.equals("/bot") || txt.equals("/bot@MireaSchedule_Bot") || txt.equals("/bot@HappyEduBot"))
            {
                sendChatActionTyping(msg_final);
                sendStartKeyboard(msg_final, true);
            }
            else if (txt.equals("/help") || txt.equals(Emoji.Question_Mark + "Помощь") ||
                    txt.equals("/help@MireaSchedule_Bot") || txt.equals("/help@HappyEduBot"))
            {
                sendChatActionTyping(msg_final);
                sendHelpKeyboard(msg_final, true);
            }
            else if (txt.equals("/settings") || txt.equals(Emoji.Gear + "Настройки") ||
                    txt.equals("/settings@MireaSchedule_Bot") || txt.equals("/settings@HappyEduBot"))
            {
                sendChatActionTyping(msg_final);
                sendSettingsKeyboard(msg_final, true);
            }
            else if (txt.equals("/admin_panel/"))
            {
                sendChatActionTyping(msg_final);
                sendAdminKeyboard(msg_final, true);
            }
            else if (txt.equals(Emoji.Left_Arrow + "Выход  из админки"))
            {
                sendChatActionTyping(msg_final);
                sendStartKeyboard(msg_final, true);
            }
            else if (txt.equals(Emoji.Keyboard + "Лог"))
            {
                sendChatActionTyping(msg_final);
                sendMsgReply(msg_final, Emoji.Keyboard + "*Лог*: Введите пароль для выполнения команды");
            }
            else if (txt.equals(Emoji.Keyboard + "Запрос"))
            {
                sendChatActionTyping(msg_final);
                sendMsgReply(msg_final, Emoji.Keyboard + "*Запрос*: Введите пароль для выполнения команды");
            }
            else if (txt.equals(Emoji.Play_Button + "Команды"))
            {
                sendChatActionTyping(msg_final);
                sendCommandKeyboard(msg_final, true);
            }
            else if (txt.equals("/close") || txt.equals(Emoji.Cross_Mark + "Закрыть"))
            {
                sendChatActionTyping(msg_final);
                sendHideKeyboard(msg_final, true);
            }
            else if (txt.equals(Emoji.Left_Arrow + "Назад к командам"))
            {
                sendChatActionTyping(msg_final);
                sendCommandKeyboard(msg_final, true);
            }
            else if (txt.equals(Emoji.Back_Arrow + "Назад"))
            {
                sendChatActionTyping(msg_final);
                sendStartKeyboard(msg_final, true);
            }
            else if (txt.equals(Emoji.Page_With_Curl + "Расписание"))
            {
                sendChatActionTyping(msg_final);
                sendScheduleKeyboard(msg_final, true);
            }
            else if (txt.equals(Emoji.World_Map + "Пара"))
            {
                sendChatActionTyping(msg_final);
                sendClassKeyboard(msg_final, true);
            }
            else if (txt.equals(Emoji.Man_Student + "Преподаватель"))
            {
                sendChatActionTyping(msg_final);
                sendTeacherKeyboard(msg_final, true);
            }
            else if (txt.equals(Emoji.Watch + "Время"))
            {
                sendChatActionTyping(msg_final);
                sendTimeKeyboard(msg_final, true);
            }
            else if (txt.equals(Emoji.Wrench + "Текущая группа"))
            {
                sendChatActionTyping(msg_final);
                if ((userDao.getUser(msg_final.getChatId()).getGroupId() != null) &&
                        (userDao.getUser(msg_final.getChatId()).getGroupId() != 0))
                    sendMsg(msg_final, groupDao.getGroup(userDao.getUser(msg_final.getChatId()).getGroupId()).getGroupName(), true);
                else
                    sendMsg(msg_final,"У вас еще не установлена группа!", true);
            }
            else if (txt.equals(Emoji.Man + "Имя"))
            {
                sendChatActionTyping(msg_final);
                sendMsgReply(msg_final,Emoji.Man + "Имя: Введите фамилию преподавателя (с большой буквы)");
            }
            else if (txt.equals(Emoji.Wrench + "Установить группу"))
            {
                sendChatActionTyping(msg_final);
                if (((userDao.getUser(msg_final.getChatId()).getGroupId() == null) ||
                        (userDao.getUser(msg_final.getChatId()).getGroupId() == 0)) &&
                                (scheduleDao.getSchedulesForUser(msg_final.getChatId()).size() == 0))
                    sendMsgReply(msg_final,Emoji.Wrench + "Установить группу: Введите группу в формате XXXX-YY-YY " +
                            "(X - буквы, Y - цифры)");
                else if ((userDao.getUser(msg_final.getChatId()).getGroupId() != null) &&
                        (userDao.getUser(msg_final.getChatId()).getGroupId() != 0))
                    sendMsgReply(msg_final,"У вас уже установлена группа " +
                            groupDao.getGroup(userDao.getUser(msg_final.getChatId()).getGroupId()).getGroupName() +
                            ". Вы точно хотите сменить группу? (В ответ пришлите \"Да\", если хотите сменить " +
                            "группу и \"Нет\", если хотите оставить ее)");
                else if (scheduleDao.getSchedulesForUser(msg_final.getChatId()).size() != 0)
                    sendMsgReply(msg_final,"У вас установлено пользовательское расписание, вы уверены, " +
                            "что хотите его удалить и установить расписание группы? (В ответ пришлите \"Да\", " +
                            "если хотите удалить расписание и \"Нет\", если хотите оставить его)");
            }
            else if (txt.equals(Emoji.Wrench + "Удалить группу"))
            {
                sendChatActionTyping(msg_final);
                if ((userDao.getUser(msg_final.getChatId()).getGroupId() == null) ||
                        (userDao.getUser(msg_final.getChatId()).getGroupId() == 0))
                {
                    sendMsg(msg_final,"Вы не привязаны к какой-либо группе", true);
                    sendSettingsKeyboard(msg_final, false);
                }
                else if ((userDao.getUser(msg_final.getChatId()).getGroupId() != null) &&
                        (userDao.getUser(msg_final.getChatId()).getGroupId() != 0))
                    sendMsgReply(msg_final,"У вас установлена группа " +
                            groupDao.getGroup(userDao.getUser(msg_final.getChatId()).getGroupId()).getGroupName() +
                            ". Вы точно хотите удалить группу? (В ответ пришлите \"Да\", если хотите удалить " +
                            "группу и \"Нет\", если хотите оставить ее)");
            }
            else if (txt.equals(Emoji.Open_Book + "Сегодня"))
            {
                sendChatActionTyping(msg_final);
                if ((scheduleDao.getSchedulesForUser(msg_final.getChatId()).size() == 0) &&
                    ((userDao.getUser(msg_final.getChatId()).getGroupId() == 0) ||
                            (userDao.getUser(msg_final.getChatId()).getGroupId() == null)))
                {
                    sendMsg(msg_final, "К вашему профилю не подключены расписания. Выберете группу в меню настроек" +
                        " или установите свое расписание (пока недоступно)", true);
                }
                else
                {
                    Calendar c = Calendar.getInstance();
                    preparationScheduleSend(msg_final, c);
                }
            }
            else if (txt.equals(Emoji.Green_Book + "Завтра"))
            {
                sendChatActionTyping(msg_final);
                if ((scheduleDao.getSchedulesForUser(msg_final.getChatId()).size() == 0) &&
                        ((userDao.getUser(msg_final.getChatId()).getGroupId() == 0) ||
                                (userDao.getUser(msg_final.getChatId()).getGroupId() == null)))
                {
                    sendMsg(msg_final, "К вашему профилю не подключены расписания. Выберете группу в меню настроек" +
                            " или установите свое расписание (пока недоступно)", true);
                }
                else
                {
                    Calendar c = Calendar.getInstance();
                    c.add(Calendar.DATE, 1);
                    preparationScheduleSend(msg_final, c);
                }
            }
            else if (txt.equals(Emoji.Orange_Book + "Дата"))
            {
                sendChatActionTyping(msg_final);
                if ((scheduleDao.getSchedulesForUser(msg_final.getChatId()).size() == 0) &&
                        ((userDao.getUser(msg_final.getChatId()).getGroupId() == 0) ||
                                (userDao.getUser(msg_final.getChatId()).getGroupId() == null)))
                {
                    sendMsg(msg_final, "К вашему профилю не подключены расписания. Выберете группу в меню настроек" +
                            " или установите свое расписание (пока недоступно)", true);
                }
                else
                {
                    sendMsgReply(msg_final, "Введите дату в формате дд.мм.гггг");
                }
            }
            else if (txt.equals(Emoji.Paperclip + "Файл расписания"))
            {
                sendChatActionTyping(msg_final);
                if ((scheduleDao.getSchedulesForUser(msg_final.getChatId()).size() == 0) &&
                        ((userDao.getUser(msg_final.getChatId()).getGroupId() == 0) ||
                                (userDao.getUser(msg_final.getChatId()).getGroupId() == null)))
                {
                    sendMsg(msg_final, "К вашему профилю не подключены расписания. Выберете группу в меню настроек" +
                            " или установите свое расписание (пока недоступно)", true);
                }
                else
                {
                    String fileName = groupDao.getGroup(userDao.getUser(msg_final.getChatId()).getGroupId()).getFileName();
                    try {
                        FileInputStream schedule = new FileInputStream("./schedule/" + fileName);
                        sendDocument(msg_final, fileName, schedule);
                    } catch (FileNotFoundException e) {
                        Main._Log.warn("Не удалось отправить файл расписания" + e);
                        sendMsg(msg_final, "К сожалению в данный момент невозможно скачать данное расписание. " +
                                "Попробуйте позже", true);
                    }
                }

            }
            else if (txt.equals(Emoji.Closed_Book + "Неделя"))
            {
                if ((scheduleDao.getSchedulesForUser(msg_final.getChatId()).size() == 0) &&
                        ((userDao.getUser(msg_final.getChatId()).getGroupId() == 0) ||
                                (userDao.getUser(msg_final.getChatId()).getGroupId() == null)))
                {
                    sendMsg(msg_final, "К вашему профилю не подключены расписания. Выберете группу в меню настроек" +
                            " или установите свое расписание (пока недоступно)", true);
                }
                else
                {
                    sendChatActionTyping(msg_final);

                    Calendar c = Calendar.getInstance();

                    if (isSemester(c))
                    {
                        Date date = c.getTime();
                        Integer dayOfWeek = 7 - (8 - c.get(Calendar.DAY_OF_WEEK))%7;
                        Integer numberOfWeek = getCurrentWeek(c);
                        if (dayOfWeek == 7)
                        {
                            numberOfWeek = numberOfWeek - 1;
                        }
                        String day = new SimpleDateFormat( "EEEE", new Locale("ru")).format(date);
                        for (int i = dayOfWeek; i <= 7; i++)
                        {
                            sendMsg(msg_final, "*" + firstUpperCase(day) + ", " + numberOfWeek + " неделя*", false);
                            sendChatActionTyping(msg_final);
                            sendScheduleForDate(msg_final, i, numberOfWeek%2, day);
                            c.add(Calendar.DATE, 1);
                            date = c.getTime();
                            day = new SimpleDateFormat("EEEE", new Locale("ru")).format(date);
                        }
                        numberOfWeek++;
                        for (int i = 1; i < dayOfWeek; i++)
                        {
                            sendMsg(msg_final, "*" + firstUpperCase(day) + ", " + numberOfWeek + " неделя*", false);
                            sendChatActionTyping(msg_final);
                            sendScheduleForDate(msg_final, i, numberOfWeek%2, day);
                            c.add(Calendar.DATE, 1);
                            date = c.getTime();
                            day = new SimpleDateFormat("EEEE", new Locale("ru")).format(date);
                        }
                    }
                    else if (isTestSession(c))
                    {
                        sendMsg(msg_final, "Сейчас идет зачетная сессия", true);
                    }
                    else if (isExamSession(c))
                    {
                        sendMsg(msg_final, "Сейчас идет экзаменационная сессия", true);
                    }
                    else
                        sendMsg(msg_final, "*Сейчас каникулы!*", true);
                }
            }
            else if (txt.equals(Emoji.Six_O_clock + "Пары"))
            {
                sendChatActionTyping(msg_final);
                Iterator<ClassTime> classTimeIterator = classTimeDao.getClassTimes().iterator();
                Integer i = 0;
                while (classTimeIterator.hasNext())
                {
                    ClassTime classTime = classTimeDao.getClassTimes().get(i);
                    sendMsg(msg_final, classTime.getClassNumber().toString() + " пара "
                            + classTime.getClassStart() + "-" + classTime.getClassStop() + "\n", false);
                    classTimeIterator.next();
                    i++;
                }
            }
            else if (txt.equals(Emoji.Pen + "Текущая"))
            {
                sendChatActionTyping(msg_final);
                Calendar c = Calendar.getInstance();
                Date date = c.getTime();
                String time = new SimpleDateFormat( "HH:mm", new Locale("ru")).format(date);
                Integer classNumber = getClassNumber(time);

                if (isSemester(c))
                {
                    Integer dayOfWeek = 7 - (8 - c.get(Calendar.DAY_OF_WEEK))%7;
                    String day = new SimpleDateFormat("EEEE").format(date);
                    Integer numberOfWeek = getCurrentWeek(c);

                    if (classNumber > 0 && classNumber <= 8)
                    {
                        sendScheduleForTime(msg_final, dayOfWeek, numberOfWeek%2, classNumber, day);
                    }
                    else
                        sendMsg(msg_final, "Сегодня пар уже нет!", true);
                }
                else if (isTestSession(c))
                {
                    sendMsg(msg_final, "Сейчас идет зачетная сессия", true);
                }
                else if (isExamSession(c))
                {
                    sendMsg(msg_final, "Сейчас идет экзаменационная сессия", true);
                }
                else
                    sendMsg(msg_final, "*Сейчас каникулы!*", true);
            }
            else if (txt.equals(Emoji.Pencil + "Следующая"))
            {
                sendChatActionTyping(msg_final);
                Calendar c = Calendar.getInstance();
                Date date = c.getTime();
                String time = new SimpleDateFormat( "HH:mm", new Locale("ru")).format(date);
                Integer classNumber = getClassNumber(time) + 1;

                if (isSemester(c))
                {
                    Integer dayOfWeek = 7 - (8 - c.get(Calendar.DAY_OF_WEEK))%7;
                    String day = new SimpleDateFormat("EEEE").format(date);
                    Integer numberOfWeek = getCurrentWeek(c);

                    if (classNumber > 0 && classNumber <= 8)
                    {
                        sendScheduleForTime(msg_final, dayOfWeek, numberOfWeek%2, classNumber, day);
                    }
                    else
                        sendMsg(msg_final, "Следующей пары сегодня уже не будет!", true);
                }
                else if (isTestSession(c))
                {
                    sendMsg(msg_final, "Сейчас идет зачетная сессия", true);
                }
                else if (isExamSession(c))
                {
                    sendMsg(msg_final, "Сейчас идет экзаменационная сессия", true);
                }
                else
                    sendMsg(msg_final, "*Сейчас каникулы!*", true);

            }
            else if (txt.equals(Emoji.Fountain_Pen + "Определенная"))
            {
                sendChatActionTyping(msg_final);
                sendMsgReply(msg_final, "Введите номер пары");
            }
            else if (txt.equals(Emoji.Three_O_clock + "Неделя"))
            {

                sendChatActionTyping(msg_final);
                Calendar c = Calendar.getInstance();
                sendMsg(msg_final, "Сейчас идет " + getCurrentWeek(c).toString() + " неделя", true);
            }
            else if (txt.equals(Emoji.Twelve_O_clock + "Сессия"))
            {
                sendChatActionTyping(msg_final);
                Calendar c = Calendar.getInstance();

                Date currentDate = c.getTime();
                if (isSemester(c))
                {

                    Date testSessionStartDate = educationDateDao.getEducationDate().getTestSessionStartDate();
                    Date examSessionStartDate = educationDateDao.getEducationDate().getExamSessionStartDate();
                    if (testSessionStartDate != null && examSessionStartDate != null)
                    {
                        int differenceTestSession = Math.abs((int)((currentDate.getTime()/(24*60*60*1000))
                                - (int)(testSessionStartDate.getTime()/(24*60*60*1000)))) + 1;
                        int differenceExamSession = Math.abs((int)((currentDate.getTime()/(24*60*60*1000))
                                - (int)(examSessionStartDate.getTime()/(24*60*60*1000)))) + 1;

                        if (differenceTestSession%10 == 1 && differenceTestSession%100 != 11)
                            sendMsg(msg_final, "До зачетной сессии " + differenceTestSession + " день",
                                    false);
                        else if ((differenceTestSession%10 == 2 || differenceTestSession%10 == 3
                                || differenceTestSession%10 == 4) && (differenceTestSession%100 != 12
                                && differenceTestSession%100 != 13 && differenceTestSession%100 != 14))
                            sendMsg(msg_final, "До зачетной сессии " + differenceTestSession + " дня" ,
                                    false);
                        else
                            sendMsg(msg_final, "До зачетной сессии " + differenceTestSession + " дней",
                                    false);

                        sendChatActionTyping(msg_final);

                        if (differenceExamSession%10 == 1 && differenceExamSession%100 != 11)
                            sendMsg(msg_final, "До экзаменационной сессии " + differenceExamSession + " день",
                                    false);
                        else if ((differenceExamSession%10 == 2 || differenceExamSession%10 == 3
                                || differenceExamSession%10 == 4) && (differenceExamSession%100 != 12
                                && differenceExamSession%100 != 13 && differenceExamSession%100 != 14))
                            sendMsg(msg_final, "До экзаменационной сессии " + differenceExamSession + " дня" ,
                                    false);
                        else
                            sendMsg(msg_final, "До экзаменационной сессии " + differenceExamSession + " дней",
                                    false);
                    }
                    else if (testSessionStartDate != null && examSessionStartDate == null)
                    {
                        int differenceTestSession = Math.abs((int)((currentDate.getTime()/(24*60*60*1000))
                                - (int)(testSessionStartDate.getTime()/(24*60*60*1000)))) + 1;
                        if (differenceTestSession%10 == 1 && differenceTestSession%100 != 11)
                            sendMsg(msg_final, "До зачетной сессии " + differenceTestSession + " день",
                                    false);
                        else if ((differenceTestSession%10 == 2 || differenceTestSession%10 == 3
                                || differenceTestSession%10 == 4) && (differenceTestSession%100 != 12
                                && differenceTestSession%100 != 13 && differenceTestSession%100 != 14))
                            sendMsg(msg_final, "До зачетной сессии " + differenceTestSession + " дня" ,
                                    false);
                        else
                            sendMsg(msg_final, "До зачетной сессии " + differenceTestSession + " дней",
                                    false);

                        sendChatActionTyping(msg_final);

                        sendMsg(msg_final, "Дата начала экзаменационной сессии еще не известна", false);
                    }
                    else if (testSessionStartDate == null && examSessionStartDate != null)
                    {
                        sendMsg(msg_final, "Дата начала зачетной сессии еще не известна", false);

                        sendChatActionTyping(msg_final);

                        int differenceExamSession = Math.abs((int)((currentDate.getTime()/(24*60*60*1000))
                                - (int)(examSessionStartDate.getTime()/(24*60*60*1000)))) + 1;
                        if (differenceExamSession%10 == 1 && differenceExamSession%100 != 11)
                            sendMsg(msg_final, "До экзаменационной сессии " + differenceExamSession + " день",
                                    false);
                        else if ((differenceExamSession%10 == 2 || differenceExamSession%10 == 3
                                || differenceExamSession%10 == 4) && (differenceExamSession%100 != 12
                                && differenceExamSession%100 != 13 && differenceExamSession%100 != 14))
                            sendMsg(msg_final, "До экзаменационной сессии " + differenceExamSession + " дня" ,
                                    false);
                        else
                            sendMsg(msg_final, "До экзаменационной сессии " + differenceExamSession + " дней",
                                    false);
                    }
                    else
                        sendMsg(msg_final, "Даты начала зачетной и экзаменационной сессии еще не известны",
                                true);

                }
                else if (isTestSession(c))
                {
                    Date examSessionStartDate = educationDateDao.getEducationDate().getExamSessionStartDate();

                    if (examSessionStartDate != null)
                    {
                        int differenceSession = Math.abs((int)((currentDate.getTime()/(24*60*60*1000))
                                - (int)(examSessionStartDate.getTime()/(24*60*60*1000)))) + 1;
                        if (differenceSession%10 == 1 && differenceSession%100 != 11)
                            sendMsg(msg_final, "Идет зачетная сессия. До экзаменационной сессии " + differenceSession
                                    + " день", false);
                        else if ((differenceSession%10 == 2 || differenceSession%10 == 3
                                || differenceSession%10 == 4) && (differenceSession%100 != 12
                                && differenceSession%100 != 13 && differenceSession%100 != 14))
                            sendMsg(msg_final, "Идет зачетная сессия. До экзаменационной сессии " + differenceSession
                                    + " дня" ,false);
                        else
                            sendMsg(msg_final, "Идет зачетная сессия. До экзаменационной сессии " + differenceSession
                                    + " дней", false);
                    }
                    else
                        sendMsg(msg_final, "Идет зачетная сессия. Дата начала экзаменационной сессии еще неизвестна",
                                true);

                }
                else if (isExamSession(c))
                {
                    Date examSessionStopDate = educationDateDao.getEducationDate().getExamSessionStopDate();

                    if (examSessionStopDate != null)
                    {
                        int differenceSession = Math.abs((int)((currentDate.getTime()/(24*60*60*1000))
                                - (int)(examSessionStopDate.getTime()/(24*60*60*1000)))) + 1;
                        if (differenceSession%10 == 1 && differenceSession%100 != 11)
                            sendMsg(msg_final, "Идет экзаменационная сессия. До конца экзаменационной сессии "
                                    + differenceSession + " день", false);
                        else if ((differenceSession%10 == 2 || differenceSession%10 == 3
                                || differenceSession%10 == 4) && (differenceSession%100 != 12
                                && differenceSession%100 != 13 && differenceSession%100 != 14))
                            sendMsg(msg_final, "Идет экзаменационная сессия. До конца экзаменационной сессии "
                                    + differenceSession + " дня" ,false);
                        else
                            sendMsg(msg_final, "Идет экзаменационная сессия. До конца экзаменационной сессии "
                                    + differenceSession + " дней", false);
                    }
                    else
                        sendMsg(msg_final, "Идет экзаменационная сессия. Дата окнчания экзаменационной сессии еще " +
                                        "неизвестна", false);
                }
                else
                {
                    Date semesterStartDate;
                    try
                    {
                        semesterStartDate = new SimpleDateFormat("dd.MM.yyyy").parse("01.09." + c.get(Calendar.YEAR));
                    }
                    catch (ParseException e)
                    {
                        Main._Log.info("Не удалось поместить дату начала семестра\n");
                        return;
                    }
                    int difference = Math.abs((int)((currentDate.getTime()/(24*60*60*1000))
                            - (int)(semesterStartDate.getTime()/(24*60*60*1000)))) + 1;
                    if (difference%10 == 1 && difference%100 != 11)
                        sendMsg(msg_final, "*Каникулы!!!*. До начала семестра " + difference + " день",
                                false);
                    else if ((difference%10 == 2 || difference%10 == 3
                            || difference%10 == 4) && (difference%100 != 12
                            && difference%100 != 13 && difference%100 != 14))
                        sendMsg(msg_final, "*Каникулы!!!*. До начала семестра " + difference + " дня" ,
                                false);
                    else
                        sendMsg(msg_final, "*Каникулы!!!*. До начала семестра " + difference + " дней",
                                false);
                }
            }
            else if (txt.equals(Emoji.Man_Pouting + "Дисциплина"))
            {
                sendChatActionTyping(msg_final);
                sendMsgReply(msg_final, "Введите название дисциплины (как в расписании)");
            }
            else if (txt.equals(Emoji.Crayon + "Аудитория") || txt.equals(Emoji.Question_Mark + "Помощь по командам") ||
                    txt.equals(Emoji.White_Question_Mark + "Помощь по настройкам") ||
                    txt.equals(Emoji.Exclamation_Question_Mark + "О боте") ||  txt.equals(Emoji.Memo + "Контакты") ||
                    txt.equals(Emoji.White_Question_Mark + "Создать свое расписание"))
            {
                sendChatActionTyping(msg_final);
                String sorryText = "К сожалению, данная функция пока *не готова*.\n" +
                                   "Но она станет доступна уже совсем *скоро*\n" +
                                   "Ждите следующих обновлений";
                sendMsg(msg_final, sorryText, true);
                sendSticker(msg_final, Sticker.Cat_Sorry.toString());
            }
        }

        /** Выполнение необходимого условия в зависимости от полученного ответа на сообщение бота */
        private void checkReplyMessage(Message msg_final)
        {
            String txt = msg_final.getText();
            String replyTxt = msg_final.getReplyToMessage().getText();

            if (replyTxt.contains(Emoji.Man + "Имя"))
            {
                sendChatActionTyping(msg_final);
                if ((teacherDao.getTeacherForSurname(txt).size() > 0))
                {
                    Iterator<Teacher> teacherIterator = teacherDao.getTeacherForSurname(txt).iterator();
                    Integer i = 0;
                    while (teacherIterator.hasNext())
                    {
                        Main._Log.info("Отправка " + (i + 1) + " пары\n");
                        Teacher teacher = teacherDao.getTeacherForSurname(txt).get(i);

                        if (teacher.getName() != null)
                        {
                            String name = teacher.getName() + " ";

                            if (teacher.getSecond_name() != null)
                            {
                                String second_name = teacher.getSecond_name();
                                Main._Log.info("Получены имя: " + name + " и отчество: " + second_name +
                                        " преподавателя: " + txt + "\n");
                                sendMsg(msg_final, txt + " " + name + second_name, false);
                            }
                            else
                            {
                                Main._Log.info("Получены имя: " + name + " преподавателя: " + txt + "\n");
                                sendMsg(msg_final, txt + " " + name, false);
                            }
                        }
                        i++;
                        teacherIterator.next();
                        sendChatActionTyping(msg_final);
                    }
                }
                else
                {
                    sendMsg(msg_final, "Преподаватель с такой фамилией не найден в базе. Пожалуйста повторите запрос.",
                            true);
                    sendChatActionTyping(msg_final);
                }
                sendTeacherKeyboard(msg_final, false);
            }
            else if (replyTxt.contains(Emoji.Wrench + "Установить группу"))
            {
                sendChatActionTyping(msg_final);
                if (groupDao.getGroupForName(txt).size() > 0)
                {
                    Integer groupId = groupDao.getGroupForName(txt).get(0).getId();
                    userDao.Merge(msg_final.getChatId(), msg_final.getChat().getUserName(), groupId);
                    sendMsg(msg_final, "Вы успешно прикреплены к группе " +
                            groupDao.getGroup(userDao.getUser(msg_final.getChatId()).getGroupId()).getGroupName(), true);
                }
                else
                {
                    sendMsg(msg_final, "Введенной группы нет в базе. Возможно вы ввели название с ошибкой или" +
                            " данную группу еще не добавили в базу", true);
                }
                sendSettingsKeyboard(msg_final, false);
            }
            else if (replyTxt.contains("установлено пользовательское расписание"))
            {
                sendChatActionTyping(msg_final);
                if (txt.toLowerCase().equals("нет"))
                {
                    sendMsg(msg_final, "Вы оставили свое расписание", true);
                }
                else if (txt.toLowerCase().equals("да"))
                {
                    scheduleDao.DeleteUserSchedule(msg_final.getChatId());
                    sendMsg(msg_final, "Пользовательское расписание *удалено*", true);
                    sendChatActionTyping(msg_final);
                    sendMsgReply(msg_final, Emoji.Wrench + "Установить группу: Введите группу в формате XXXX-YY-YY " +
                            "(X - буквы, Y - цифры)");
                }
                else
                    sendMsgReply(msg_final,"Вы ввели некорректный ответ. Введите *Да* или *Нет*. " +
                            "У вас установлено пользовательское расписание, вы уверены, " +
                            "что хотите его удалить и установить расписание группы?");
                sendSettingsKeyboard(msg_final, false);
            }
            else if (replyTxt.contains("уже установлена группа"))
            {
                sendChatActionTyping(msg_final);
                if (txt.toLowerCase().equals("нет"))
                {
                    sendMsg(msg_final, "Вы оставили расписание группы " +
                            groupDao.getGroup(userDao.getUser(msg_final.getChatId()).getGroupId()).getGroupName(), true);
                    sendChatActionTyping(msg_final);
                    sendSettingsKeyboard(msg_final, false);
                }
                else if (txt.toLowerCase().equals("да"))
                {
                    sendMsgReply(msg_final, Emoji.Wrench + "Установить группу: Введите группу в формате XXXX-YY-YY " +
                            "(X - буквы, Y - цифры)");
                }
                else
                    sendMsgReply(msg_final,"Вы ввели некорректный ответ. Введите *Да* или *Нет*. У вас уже установлена группа " +
                            groupDao.getGroup(userDao.getUser(msg_final.getChatId()).getGroupId()).getGroupName() +
                            ". Вы точно хотите сменить группу?");
            }
            else if (replyTxt.contains("хотите удалить группу"))
            {
                sendChatActionTyping(msg_final);
                if (txt.toLowerCase().equals("да"))
                {
                    userDao.Merge(msg_final.getChatId(), msg_final.getChat().getUserName(), null);
                    sendMsg(msg_final, "Вы успешно отсоеденены от группы!", true);
                    sendChatActionTyping(msg_final);
                    sendSettingsKeyboard(msg_final, false);
                }
                else if (txt.toLowerCase().equals("нет"))
                {
                    sendMsg(msg_final, "Вы оставили расписание группы " +
                            groupDao.getGroup(userDao.getUser(msg_final.getChatId()).getGroupId()).getGroupName(), true);
                    sendChatActionTyping(msg_final);
                    sendSettingsKeyboard(msg_final, false);
                }
                else
                    sendMsgReply(msg_final,"Вы ввели некорректный ответ. Введите *Да* или *Нет*. У вас установлена группа " +
                            groupDao.getGroup(userDao.getUser(msg_final.getChatId()).getGroupId()).getGroupName() +
                            ". Вы точно хотите удалить группу?");

            }
            else if (replyTxt.contains("Введите дату в формате"))
            {
                sendChatActionTyping(msg_final);

                if (checkDate(txt))
                {
                    Date time;
                    try
                    {
                        time = new SimpleDateFormat("dd.MM.yyyy").parse(txt);

                    }
                    catch (ParseException e) {
                        Main._Log.warn("Не удалось поместить дату " + e.toString() + "\n");
                        sendMsgReply(msg_final, "Вы ввели дату *неверно*! Введите дату в формате дд.мм.гггг");
                        return;
                    }
                    Calendar c = Calendar.getInstance();
                    c.setTime(time);
                    preparationScheduleSend(msg_final, c);
                    sendScheduleKeyboard(msg_final, false);
                }
                else
                    sendMsgReply(msg_final, "Вы ввели дату *неверно*! Введите дату в формате дд.мм.гггг");

            }
            else if (replyTxt.contains("Введите номер пары"))
            {
                sendChatActionTyping(msg_final);
                Calendar c = Calendar.getInstance();
                Date date = c.getTime();
                Integer classNumber = Integer.parseInt(txt);
                String day = new SimpleDateFormat("EEEE").format(date);

                if (isSemester(c))
                {
                    Integer dayOfWeek = 7 - (8 - c.get(Calendar.DAY_OF_WEEK)) % 7;
                    Integer numberOfWeek = 1;

                    if (classNumber > 0 && classNumber <= 8)
                    {
                        sendScheduleForTime(msg_final, dayOfWeek, numberOfWeek, classNumber, day);
                        sendChatActionTyping(msg_final);
                        sendClassKeyboard(msg_final, false);
                    }
                    else
                    {
                        sendMsgReply(msg_final, "Такой пары *не бывает*! Введите номер пары");
                    }
                }
                else if (isTestSession(c))
                {
                    sendMsg(msg_final, "Сейчас идет зачетная сессия", true);
                }
                else if (isExamSession(c))
                {
                    sendMsg(msg_final, "Сейчас идет экзаменационная сессия", true);
                }
                else
                    sendMsg(msg_final, "*Сейчас каникулы!*", true);
            }
            else if (replyTxt.contains(Emoji.Card_Index + "Регистрация:\n"))
            {
                sendChatActionTyping(msg_final);
                if (txt.toLowerCase().equals("нет"))
                {
                    userDao.Merge(msg_final.getChatId(), msg_final.getFrom().getUserName(), null);
                    Main._Log.info("Пользователь " + userDao.getUser(msg_final.getChatId()).getChatName() +
                            " с chatId = " + msg_final.getChatId() + " добавлен в базу\n");
                    sendMsg(msg_final, "Вы успешно зарегистрированы! Если вы захотите ввести группу в дальнейшем," +
                            " то можете это сделать в меню настроек", true);
                    sendStartKeyboard(msg_final, false);
                }
                else if (!(txt.toLowerCase().equals("нет")) && groupDao.getGroupForName(txt).size() > 0)
                {
                    Integer groupId = groupDao.getGroupForName(txt).get(0).getId();
                    userDao.Merge(msg_final.getChatId(), msg_final.getChat().getUserName(), groupId);
                    sendMsg(msg_final, "Вы успешно зарегистрированы и прикреплены к группе " +
                            groupDao.getGroup(userDao.getUser(msg_final.getChatId()).getGroupId()).getGroupName(), true);
                    Main._Log.info("Пользователь " + userDao.getUser(msg_final.getChatId()).getChatName() +
                            " с chatId = " + msg_final.getChatId() + " добавлен в базу и прикреплен к группе "
                            + groupDao.getGroup(userDao.getUser(msg_final.getChatId()).getGroupId()).getGroupName());
                    sendChatActionTyping(msg_final);
                    sendStartKeyboard(msg_final, false);
                }
                else if (!(txt.toLowerCase().equals("нет")) && groupDao.getGroupForName(txt).size() == 0)
                    sendMsgReply(msg_final, Emoji.Card_Index + "Регистрация:\nК сожелению, введенной вами группы не существует или она еще " +
                            "не внесена в базу. Попробуйте еще раз или напишите слово \"Нет\", чтобы установить группу позднее");
            }
            else if (replyTxt.contains("Введите название дисциплины"))
            {
                sendChatActionTyping(msg_final);
                if (teacherDao.getTeacherForSubject(txt).size() != 0)
                {
                    if (teacherDao.getTeacherForSubject(txt).size() == 1)
                    {
                        Teacher teacher = teacherDao.getTeacherForSubject(txt).get(0);
                        if (teacher.getSurname() == null || teacher.getSurname().equals(""))
                        {
                            sendMsg(msg_final, "Преподавателей данного предмета не найдено, так как они " +
                                    "не указаны в расписании", true);
                            sendChatActionTyping(msg_final);
                            sendTeacherKeyboard(msg_final, false);
                            return;
                        }
                        else
                        {
                            getTeacherBySubject(txt, msg_final);
                            sendTeacherKeyboard(msg_final, false);
                        }
                    }
                    getTeacherBySubject(txt, msg_final);
                    sendTeacherKeyboard(msg_final, false);
                }
                else
                {
                    if (subjectDao.getSubjectForName(txt).size() == 0)
                    {
                        sendMsg(msg_final, "Преподавателей данного предмета не найдено. Боюсь вы неправильно " +
                                "ввели название предмета", true);
                        sendChatActionTyping(msg_final);
                        sendTeacherKeyboard(msg_final, false);
                    }
                }
            }
            else if (replyTxt.contains(Emoji.Keyboard + "Лог") && replyTxt.contains("Введите пароль"))
            {
                if (txt.equals("/*Какой-то пароль*/"))
                {
                    sendMsg(msg_final,"Пароль введен верно", true);
                    try
                    {
                        FileInputStream log = new FileInputStream("./log4j2.log");
                        sendChatActionDocument(msg_final);
                        sendDocument(msg_final, "log4j2.log", log);
                    }
                    catch (FileNotFoundException e)
                    {
                        sendMsg(msg_final,"Не получилось отправить файл лога" + e, true);
                    }
                    try
                    {
                        FileInputStream nohup = new FileInputStream("./nohup.out");
                        sendChatActionDocument(msg_final);
                        sendDocument(msg_final, "nohup.out", nohup);
                    }
                    catch (FileNotFoundException e)
                    {
                        sendMsg(msg_final,"Не получилось отправить файл вывода в консоль" + e, true);
                    }

                    sendAdminKeyboard(msg_final, false);
                }
                else
                {
                    sendMsg(msg_final, Emoji.Keyboard + "*Лог*: Пароль введен неверно. Введите пароль", true);
                }
            }
            else if (replyTxt.contains(Emoji.Keyboard + "Запрос") && replyTxt.contains("Введите пароль"))
            {
                if (txt.equals("/*Какой-то пароль*/"))
                {
                    sendMsg(msg_final,"Пароль введен верно", true);
                    sendMsgReply(msg_final, Emoji.Keyboard + "*Запрос*: Введите свой запрос");
                }
                else
                {
                    sendMsgReply(msg_final, Emoji.Keyboard + "*Запрос*: Пароль введен неверно. Введите пароль");
                }
            }
            else if (replyTxt.contains("Введите свой запрос"))
            {
                sendChatActionTyping(msg_final);
                if (txt.toLowerCase().contains("select"))
                {
                    try
                    {
                        Integer start = 0;
                        Integer stop = 100;
                        Integer size = jdbcTemplate.queryForList(txt, String.class).size();
                        if (size>0)
                        {
                            String log_message;
                            List<String> list = jdbcTemplate.queryForList(txt, String.class);
                            sendMsg(msg_final, "Результат запроса: ", true);
                            while (start < size)
                            {
                                log_message = "";
                                for (int i = start; i < stop; i++)
                                {
                                    log_message = log_message + list.get(i) + "\n";
                                }
                                sendChatActionTyping(msg_final);
                                sendMsg(msg_final, log_message, false);
                                start = stop;
                                if (size-stop > 100)
                                    stop +=100;
                                else
                                    stop = size;
                            }
                        }
                        else
                            sendMsg(msg_final, "Запрос выдал нулевой результат", true);
                    }
                    catch (Exception e)
                    {
                        String error = e.toString();
                        error = error.replace("*", " ");
                        sendMsg(msg_final, "Запрос выполнен с ошибкой: \n" + error, true);
                        sendChatActionTyping(msg_final);
                        sendMsgReply(msg_final, "Хотите ввести новый запрос?");
                    }
                }
                else if (txt.toLowerCase().contains("update") || txt.toLowerCase().contains("delete") ||
                        txt.toLowerCase().contains("merge") || txt.toLowerCase().contains("insert"))
                {
                    try
                    {
                        jdbcTemplate.update(txt);
                        sendMsg(msg_final, "Запрос выполнен успешно", true);
                    }
                    catch (Exception e)
                    {
                        String error = e.toString();
                        error = error.replace("*", " ");
                        sendMsg(msg_final, "Запрос выполнен с ошибкой: \n" + error, true);
                        sendChatActionTyping(msg_final);
                        sendMsgReply(msg_final, "Хотите ввести новый запрос?");
                    }
                }

            }
            else if (replyTxt.contains("Хотите ввести новый запрос?"))
            {
                sendChatActionTyping(msg_final);
                if (txt.toLowerCase().equals("да"))
                    sendMsgReply(msg_final, Emoji.Keyboard + "*Запрос*: Введите свой запрос");
                else if (txt.toLowerCase().equals("нет"))
                    sendAdminKeyboard(msg_final, true);
                else
                    sendMsgReply(msg_final, "Некорректный ответ. Хотите ввести новый запрос?");
            }
        }

        /** Отправка расписания по дате */
        private void sendScheduleForDate(Message msg_final, Integer dayOfWeek, Integer numberOfWeek, String day)
        {
            if (numberOfWeek == 0)
                numberOfWeek = 2;
            if ((userDao.getUser(msg_final.getChatId()).getGroupId() != 0) &&
                    (userDao.getUser(msg_final.getChatId()).getGroupId() != null))
            {
                Integer groupId = userDao.getUser(msg_final.getChatId()).getGroupId();
                Main._Log.info("Отправка блока расписаний для группы "
                        + groupDao.getGroup(userDao.getUser(msg_final.getChatId()).getGroupId()).getGroupName()
                        + " для дня недели " + day + ". Количество пар равно "
                        + scheduleDao.getSchedulesForGroup(groupId, numberOfWeek,dayOfWeek, msg_final.getChatId()).size() + "\n");

                if (scheduleDao.getSchedulesForGroup(groupId, numberOfWeek,dayOfWeek, msg_final.getChatId()).size() > 0)
                {
                    Iterator<Schedule> scheduleIterator = scheduleDao.getSchedulesForGroup(groupId, numberOfWeek,
                            dayOfWeek, msg_final.getChatId()).iterator();
                    Integer i = 0;
                    while (scheduleIterator.hasNext()) {
                        sendChatActionTyping(msg_final);
                        Main._Log.info("Отправка " + (i+1) + " пары\n");
                        Schedule schedule = scheduleDao.getSchedulesForGroup(groupId, numberOfWeek,
                                dayOfWeek, msg_final.getChatId()).get(i);
                        String subjectName = "";
                        String subjectType = "";
                        String className = "";
                        String surname = "";
                        String name = "";
                        String second_name = "";
                        String classTime;

                        if (schedule.getSubjectName() != null)
                            subjectName = schedule.getSubjectName() + " ";

                        if (schedule.getTypeName() != null)
                            subjectType = schedule.getTypeName() + " ";

                        if (schedule.getClassName() != null)
                            className = schedule.getClassName() + " ";

                        if (schedule.getSurname() != null)
                            surname = schedule.getSurname() + " ";

                        if (schedule.getName() != null)
                            name = schedule.getName() + " ";

                        if (schedule.getSecond_name() != null)
                            second_name = schedule.getSecond_name();

                        classTime = schedule.getClassNumber().toString() + " пара (" + schedule.getClassStart()
                                + "-" + schedule.getClassStop() + ")\n";

                        sendChatActionTyping(msg_final);
                        sendMsg(msg_final, classTime + subjectName + subjectType + className + surname
                                + name + second_name, false);
                        i++;
                        scheduleIterator.next();
                    }
                }
                else sendMsg(msg_final, "Пары дома!", false);
            }
            /**else if (scheduleDao.getSchedulesForUser(msg_final.getChatId()).size() != 0)
            {
                if (scheduleDao.getSchedulesForUserForDay(msg_final.getChatId(), dayOfWeek,1).size() > 0)
                {

                }
                else sendMsg(msg_final, "Пары дома!", false);
            }*/ //TODO: Закончить фрагмент для расписания пользователя
        }

        /** Подготовка к отправке расписания */
        private void preparationScheduleSend(Message msg_final, Calendar c)
        {
            if (isSemester(c))
            {
                Date date = c.getTime();
                String day = new SimpleDateFormat( "EEEE", new Locale("ru")).format(date);
                Integer dayOfWeek = 7 - (8 - c.get(Calendar.DAY_OF_WEEK))%7;
                Integer numberOfWeek = getCurrentWeek(c);
                if (dayOfWeek == 7)
                {
                    numberOfWeek = numberOfWeek - 1;
                }
                sendMsg(msg_final, "*" + firstUpperCase(day) + ", " + numberOfWeek + " неделя*", false);
                sendChatActionTyping(msg_final);
                sendScheduleForDate(msg_final, dayOfWeek, numberOfWeek%2, day);
            }
            else if (isTestSession(c))
            {
                sendMsg(msg_final, "Сейчас идет зачетная сессия", true);
            }
            else if (isExamSession(c))
            {
                sendMsg(msg_final, "Сейчас идет экзаменационная сессия", true);
            }
            else
                sendMsg(msg_final, "*Сейчас каникулы!*", true);
        }

        /** Отправка номера пары */
        private Integer getClassNumber(String time)
        {
            Date timeStart = null;
            Date timeStop = null;
            Date date = null;
            Integer classNumber = 0;
            for (int i = 1; i <= 8; i++)
            {
                String classStart = classTimeDao.getClassTime(i).getClassStart();
                String classStop = classTimeDao.getClassTime(i).getClassStop();
                SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                try
                {
                    timeStart = format.parse(classStart);
                    timeStop = format.parse(classStop);
                    date = format.parse(time);
                }
                catch (ParseException e)
                {
                    Main._Log.warn("Время не переведено в формат дата:\n" + e.toString() + "\n");
                }
                if (((date.after(timeStart)) || date.equals(timeStart)) && ((date.before(timeStop)) || date.equals(timeStop)))
                    classNumber = i;
            }
            return classNumber;
        }

        /** Отправка расписания конкретной пары */
        private void sendScheduleForTime(Message msg_final, Integer dayOfWeek, Integer numberOfWeek, Integer classNumber, String day)
        {
            if (numberOfWeek == 0)
                numberOfWeek = 2;
            if ((userDao.getUser(msg_final.getChatId()).getGroupId() != 0) &&
                    (userDao.getUser(msg_final.getChatId()).getGroupId() != null))
            {
                Integer groupId = userDao.getUser(msg_final.getChatId()).getGroupId();
                Main._Log.info("Отправка расписания " + classNumber + " пары для группы "
                        + groupDao.getGroup(userDao.getUser(msg_final.getChatId()).getGroupId()).getGroupName()
                        + " для дня недели " + day + "\n");
                Boolean classNumberContain = false;
                Iterator<Schedule> scheduleIterator = scheduleDao.getSchedulesForGroup(groupId, numberOfWeek,
                        dayOfWeek, msg_final.getChatId()).iterator();
                Integer i = 0;
                Integer scheduleClassNumber = 0;
                while (!classNumberContain && scheduleIterator.hasNext())
                {
                    Schedule schedule = scheduleDao.getSchedulesForGroup(groupId, numberOfWeek, dayOfWeek, msg_final.getChatId()).get(i);
                    if (schedule.getClassNumber().equals(classNumber))
                    {
                        classNumberContain = true;
                        scheduleClassNumber = i;
                    }
                    scheduleIterator.next();
                    i++;
                }

                if (scheduleDao.getSchedulesForGroup(groupId, numberOfWeek,dayOfWeek, msg_final.getChatId()).
                        get(scheduleDao.getSchedulesForGroup(groupId, numberOfWeek,dayOfWeek,
                                msg_final.getChatId()).size()-1).getClassNumber()
                        >= classNumber && classNumberContain)
                {
                    Schedule schedule = scheduleDao.getSchedulesForGroup(groupId, numberOfWeek, dayOfWeek,
                            msg_final.getChatId()).get(scheduleClassNumber);

                        String subjectName = "";
                        String subjectType = "";
                        String className = "";
                        String surname = "";
                        String name = "";
                        String second_name = "";
                        String classTime;

                        if (schedule.getSubjectName() != null)
                            subjectName = schedule.getSubjectName() + " ";

                        if (schedule.getTypeName() != null)
                            subjectType = schedule.getTypeName() + " ";

                        if (schedule.getClassName() != null)
                            className = schedule.getClassName() + " ";

                        if (schedule.getSurname() != null)
                            surname = schedule.getSurname() + " ";

                        if (schedule.getName() != null)
                            name = schedule.getName() + " ";

                        if (schedule.getSecond_name() != null)
                            second_name = schedule.getSecond_name();

                        classTime = schedule.getClassNumber().toString() + " пара (" + schedule.getClassStart()
                                + "-" + schedule.getClassStop() + ")\n";

                        sendChatActionTyping(msg_final);
                        sendMsg(msg_final, classTime + subjectName + subjectType + className + surname
                                + name + second_name, true);
                }
                else sendMsg(msg_final,  "Сегодня у вас нет " + classNumber + " пары!", true);
            }
            /**else if (scheduleDao.getSchedulesForUser(msg_final.getChatId()).size() != 0)
            {
                if (scheduleDao.getSchedulesForUserForDay(msg_final.getChatId(), numberOfWeek,dayOfWeek).size() >= classNumber)
                {

                }
                else sendMsg(msg_final, "Сегодня у вас нет " + classNumber + " пары!", true);
            }*/ //TODO: Закончить фрагмент для расписания пользователя
            else if ((scheduleDao.getSchedulesForUser(msg_final.getChatId()).size() == 0) &&
                    ((userDao.getUser(msg_final.getChatId()).getGroupId() == 0) ||
                            (userDao.getUser(msg_final.getChatId()).getGroupId() == null)))
            {
                sendMsg(msg_final, "К вашему профилю не подключены расписания. Выберете группу в меню настроек" +
                        " или установите свое расписание (пока недоступно)", true);
            }
        }

        private void getTeacherBySubject(String txt, Message msg_final)
        {
            Main._Log.info("Отправка преподавателей для дисциплины " + txt + "\n");
            Iterator<Teacher> teacherIterator = teacherDao.getTeacherForSubject(txt).iterator();
            Integer i = 0;
            while (teacherIterator.hasNext())
            {
                Teacher teacher = teacherDao.getTeacherForSubject(txt).get(i);
                String teacherName = "";
                String teacherSurname = "";
                String teacherSecond_Name = "";

                if (teacher.getSurname() != null && !teacher.getSurname().equals(""))
                    teacherSurname = teacher.getSurname() + " ";

                if (teacher.getName() != null && !teacher.getName().equals(""))
                    teacherName = teacher.getName() + " ";

                if (teacher.getSecond_name() != null && !teacher.getSecond_name().equals(""))
                    teacherSecond_Name = teacher.getSecond_name();

                sendChatActionTyping(msg_final);
                if (!teacherSurname.equals(""))
                    sendMsg(msg_final, teacherSurname + teacherName + teacherSecond_Name, false);

                i++;
                teacherIterator.next();
            }
        }

        /** Метод отправки сообщений */
        private void sendMsg(Message msg, String text, Boolean replyMod)
        {
            SendMessage s = new SendMessage();
            s.enableMarkdown(true);
            s.setChatId(msg.getChatId());
            if (replyMod)
                s.setReplyToMessageId(msg.getMessageId());
            s.setText(text);
            try
            {
                execute(s);
                Main._Log.info("Отправлено сообщение: '" + s.getText() + "'\n");
            }
            catch (TelegramApiException e)
            {
                Main._Log.warn("Сообщение не отправлено: \n" + e.toString() + "\n");
            }
        }

        /** Метод отправки сообщений c принудительным ответом для пользователя*/
        private void sendMsgReply(Message msg, String text)
        {
            ForceReplyKeyboard forceReplyKeyboard = new ForceReplyKeyboard();
            forceReplyKeyboard.getForceReply();
            forceReplyKeyboard.setSelective(true);

            SendMessage s = new SendMessage();
            s.enableMarkdown(true);
            s.setChatId(msg.getChatId());
            s.setText(text);
            s.setReplyToMessageId(msg.getMessageId());
            s.setReplyMarkup(forceReplyKeyboard);
            try
            {
                execute(s);
                Main._Log.info("Отправлено сообщение с ответом: '" + s.getText() + "'\n");
            }
            catch (TelegramApiException e)
            {
                Main._Log.warn("Сообщение с ответом не отправлено: \n" + e.toString() + "\n");
            }
        }

        /** Метод отправки фото */
        private void sendPhoto(Message msg, String photo)
        {
            SendPhoto ph = new SendPhoto();
            ph.setChatId(msg.getChatId());
            ph.setPhoto(photo);
            try
            {
                sendPhoto(ph);
                Main._Log.info("Отправлено фото\n");

            } catch (TelegramApiException e) {
                Main._Log.warn("Фото не было отправлено: \n" + e.toString() + "\n");
            }
        }

        /** Метод отправки новых документов */
        private void sendDocument(Message msg, String name, InputStream document)
        {
            SendDocument sd = new SendDocument();
            sd.setChatId(msg.getChatId());
            sd.setNewDocument(name, document);
            try
            {
                sendDocument(sd);
                Main._Log.info("Документ отправлен\n");
            }
            catch (TelegramApiException e)
            {
                Main._Log.warn("Документ не был отправлен: \n" + e.toString() + "\n");
            }
        }

        /**Отправка активности печати в телеграме*/
        private void sendChatActionTyping(Message msg)
        {
            SendChatAction sca = new SendChatAction();
            sca.setChatId(msg.getChatId());
            sca.setAction(ActionType.TYPING);
            try
            {
                execute(sca);
                Main._Log.info("Активность печати отправлена\n");
            }
            catch (TelegramApiException e)
            {
                Main._Log.warn("Активность печати не была отправлена: \n" + e.toString() + "\n");
            }
        }

        /**Отправка активности отправки файла в телеграме*/
        private void sendChatActionDocument(Message msg)
        {
            SendChatAction sca = new SendChatAction();
            sca.setChatId(msg.getChatId());
            sca.setAction(ActionType.UPLOADDOCUMENT);
            try
            {
                execute(sca);
                Main._Log.info("Активность загрузки документа отправлена\n");
            }
            catch (TelegramApiException e)
            {
                Main._Log.warn("Активность загрузки документа не была отправлена: \n" + e.toString() + "\n");
            }
        }

        /**Отправка активности отправки фото в телеграме*/
        private void sendChatActionPhoto(Message msg)
        {
            SendChatAction sca = new SendChatAction();
            sca.setChatId(msg.getChatId());
            sca.setAction(ActionType.UPLOADPHOTO);
            try
            {
                execute(sca);
                Main._Log.info("Активность загрузки фото отправлена\n");
            }
            catch (TelegramApiException e)
            {
                Main._Log.warn("Активность загрузки фото не была отправлена: \n" + e.toString() + "\n");
            }
        }

        /** Метод отправки стикеров*/
        private void sendSticker(Message msg, String sticker)
        {
            SendSticker s = new SendSticker();
            s.setChatId(msg.getChatId());
            s.setSticker(sticker);
            try
            {
                sendSticker(s);
                Main._Log.info("Отправлен стикер\n");

            } catch (TelegramApiException e) {
                Main._Log.warn("Стикер не был отправлен" + e.toString() + "\n");
            }
        }

        /** Метод отправки стартовой клавиатуры */
        private void sendStartKeyboard(Message msg, Boolean replyMod)
        {
            SendMessage s = new SendMessage();
            s.enableMarkdown(true);
            s.setChatId(msg.getChatId());
            if (replyMod)
                s.setReplyToMessageId(msg.getMessageId());
            s.setText("*Добро пожаловать в \"Расписание МИРЭА\"!*\n" +
                    "*Выберите одну из опций:*\n" +
                    Emoji.Play_Button + "*Команды:* Начните взаимдействовать с ботом!\n" +
                    Emoji.Gear + "*Настройки:* Настройте бота под себя\n" +
                    Emoji.Question_Mark + "*Помощь:* Если вы не знаете, что делать\n" +
                    Emoji.Cross_Mark + "*Закрыть:* Закрывает меню");

            /** Cоздаем клавиатуру */
            ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
            s.setReplyMarkup(replyKeyboardMarkup);

            /** Устанавливаем свойства */

            /** Выборочное открытие клавиатуры (открывается только у челоевка, который ее запросил) */
            replyKeyboardMarkup.setSelective(true);
            /** Подгонка размера клавиатуры по высоте */
            replyKeyboardMarkup.setResizeKeyboard(true);
            /** Закрытие клавиатуры после единократного использования */
            replyKeyboardMarkup.setOneTimeKeyboard(false);

            /** Создаем список строк клавиатуры */
            List<KeyboardRow> keyboard = new ArrayList<>();

            /** Первая строчка клавиатуры */
            KeyboardRow keyboardFirstRow = new KeyboardRow();
            /** Добавляем кнопки в первую строчку клавиатуры */
            keyboardFirstRow.add(new KeyboardButton(Emoji.Play_Button + "Команды"));

            /** Вторая строчка клавиатуры */
            KeyboardRow keyboardSecondRow = new KeyboardRow();
            /** Добавляем кнопки во вторую строчку клавиатуры */
            keyboardSecondRow.add(new KeyboardButton(Emoji.Gear + "Настройки"));
            keyboardSecondRow.add(new KeyboardButton(Emoji.Question_Mark + "Помощь"));
            keyboardSecondRow.add(new KeyboardButton(Emoji.Cross_Mark + "Закрыть"));

            /** Добавляем все строчки клавиатуры в список */
            keyboard.add(keyboardFirstRow);
            keyboard.add(keyboardSecondRow);

            /** Устанваливаем этот список нашей клавиатуре */
            replyKeyboardMarkup.setKeyboard(keyboard);
            try
            {
                execute(s);
                Main._Log.info("Стартовая клавиатура отправлена\n");
            }
            catch (TelegramApiException e)
            {
                Main._Log.warn("Стартовая клавиатура не отправлена: \n" + e.toString() + "\n", e);
            }
        }

        /** Метод отправки клавиатуры помощи */
        private void sendHelpKeyboard(Message msg, Boolean replyMod)
        {
            SendMessage s = new SendMessage();
            s.enableMarkdown(true);
            s.setChatId(msg.getChatId());
            if (replyMod)
                s.setReplyToMessageId(msg.getMessageId());
            s.setText("*Вы находитесь в меню помощи*\n" +
                    "*Выберите одну из опций:*\n" +
                    Emoji.Question_Mark + "*Помощь по командам:* Узнайте какие можно использовать команды\n" +
                    Emoji.White_Question_Mark + "*Помощь по настройкам:* Узнайте, что можно настроить\n" +
                    Emoji.Exclamation_Question_Mark + "*О боте:* Информация о боте\n" +
                    Emoji.Back_Arrow + "*Назад:* Возвращает в стартовое меню\n" +
                    Emoji.Cross_Mark + "*Закрыть:* Закрывает меню");

            /** Cоздаем клавиатуру */

            ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
            s.setReplyMarkup(replyKeyboardMarkup);

            /** Устанавливаем свойства */

            /** Выборочное открытие клавиатуры (открывается только у челоевка, который ее запросил) */
            replyKeyboardMarkup.setSelective(true);
            /** Подгонкп размера клавиатуры по высоте */
            replyKeyboardMarkup.setResizeKeyboard(true);
            /** Закрытие клавиатуры после единократного использования */
            replyKeyboardMarkup.setOneTimeKeyboard(false);

            /** Создаем список строк клавиатуры */
            List<KeyboardRow> keyboard = new ArrayList<>();

            /** Первая строчка клавиатуры */
            KeyboardRow keyboardFirstRow = new KeyboardRow();
            /** Добавляем кнопки в первую строчку клавиатуры */
            keyboardFirstRow.add(new KeyboardButton(Emoji.Question_Mark + "Помощь по командам"));

            /** Вторая строчка клавиатуры */
            KeyboardRow keyboardSecondRow = new KeyboardRow();
            /** Добавляем кнопки во вторую строчку клавиатуры */
            keyboardSecondRow.add(new KeyboardButton(Emoji.White_Question_Mark + "Помощь по настройкам"));

            /** Третья строчка клавиатуры */
            KeyboardRow keyboardThirdRow = new KeyboardRow();
            /** Добавляем кнопки в третью строчку клавиатуры */
            keyboardThirdRow.add(new KeyboardButton(Emoji.Exclamation_Question_Mark + "О боте"));
            keyboardThirdRow.add(new KeyboardButton(Emoji.Back_Arrow + "Назад"));
            keyboardThirdRow.add(new KeyboardButton(Emoji.Cross_Mark + "Закрыть"));

            /** Добавляем все строчки клавиатуры в список */
            keyboard.add(keyboardFirstRow);
            keyboard.add(keyboardSecondRow);
            keyboard.add(keyboardThirdRow);

            /** Устанваливаем этот список нашей клавиатуре */
            replyKeyboardMarkup.setKeyboard(keyboard);
            try
            {
                execute(s);
                Main._Log.info("Отправлена клавиатура помощи\n");
            }
            catch (TelegramApiException e)
            {
                Main._Log.warn("Клавиатура помощи не отправлена: \n" + e.toString() + "\n");
            }
        }

        /** Метод отправки клавиатуры настроек */
        private void sendSettingsKeyboard(Message msg, Boolean replyMod)
        {
            SendMessage s = new SendMessage();
            s.enableMarkdown(true);
            s.setChatId(msg.getChatId());
            if (replyMod)
                s.setReplyToMessageId(msg.getMessageId());
            s.setText("*Вы находитесь в меню настроек*\n" +
                    "*Выберите одну из опций:*\n" +
                    Emoji.Wrench + "*Установить группу:* задать группу, расписание которой вам будет приходить\n" +
                    Emoji.Wrench + "*Удалить группу:* удалить группу, расписание которой вам приходит\n" +
                    Emoji.Wrench + "*Текущая группа:* вывод навзания группы, расписание которой вам приходит\n" +
                    Emoji.White_Question_Mark + "*Свое расписание:* создать свое распсиание\n" +
                    Emoji.Back_Arrow + "*Назад:* В стартовое меню\n" +
                    Emoji.Cross_Mark + "*Закрыть:* Закрывает меню");

            /** Cоздаем клавиатуру */

            ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
            s.setReplyMarkup(replyKeyboardMarkup);

            /** Устанавливаем свойства */

            /** Выборочное открытие клавиатуры (открывается только у челоевка, который ее запросил) */
            replyKeyboardMarkup.setSelective(true);
            /** Подгонка размера клавиатуры по высоте */
            replyKeyboardMarkup.setResizeKeyboard(true);
            /** Закрытие клавиатуры после единократного использования */
            replyKeyboardMarkup.setOneTimeKeyboard(false);

            /** Создаем список строк клавиатуры */
            List<KeyboardRow> keyboard = new ArrayList<>();

            /** Первая строчка клавиатуры */
            KeyboardRow keyboardFirstRow = new KeyboardRow();
            /** Добавляем кнопки в первую строчку клавиатуры */
            keyboardFirstRow.add(new KeyboardButton(Emoji.Wrench + "Установить группу"));
            keyboardFirstRow.add(new KeyboardButton(Emoji.Wrench + "Удалить группу"));

            /** Вторая строчка клавиатуры */
            KeyboardRow keyboardSecondRow = new KeyboardRow();
            /** Добавляем кнопки во вторую строчку клавиатуры */
            keyboardSecondRow.add(new KeyboardButton(Emoji.Wrench + "Текущая группа"));
            keyboardSecondRow.add(new KeyboardButton( Emoji.White_Question_Mark + "Создать свое расписание"));

            /** Третья строчка клавиатуры */
            KeyboardRow keyboardThirdRow = new KeyboardRow();
            /** Добавляем кнопки в третью строчку клавиатуры */
            keyboardThirdRow.add(new KeyboardButton(Emoji.Back_Arrow + "Назад"));
            keyboardThirdRow.add(new KeyboardButton(Emoji.Cross_Mark + "Закрыть"));

            /** Добавляем все строчки клавиатуры в список */
            keyboard.add(keyboardFirstRow);
            keyboard.add(keyboardSecondRow);
            keyboard.add(keyboardThirdRow);

            /** Устанваливаем этот список нашей клавиатуре */
            replyKeyboardMarkup.setKeyboard(keyboard);
            try
            {
                execute(s);
                Main._Log.info("Отправлена клавиатура настроек\n");
            }
            catch (TelegramApiException e)
            {
                Main._Log.warn("Клавиатура настроек не отправлена: \n" + e.toString() + "\n");
            }
        }

        /** Метод отправки клавиатуры команд */
        private void sendCommandKeyboard(Message msg, Boolean replyMod)
        {
            SendMessage s = new SendMessage();
            s.enableMarkdown(true);
            s.setChatId(msg.getChatId());
            if (replyMod)
                s.setReplyToMessageId(msg.getMessageId());
            s.setText("*Вы находитесь в меню команд*\n" +
                    "*Выберите одну из опций:*\n" +
                    Emoji.Page_With_Curl + "*Расписание:* Меню, в котором можно получить расписание\n" +
                    Emoji.World_Map + "*Пара:* Меню, в котором вы можете узнать, где проходит пара\n" +
                    Emoji.Man_Student + "*Преподаватель:* Меню, в котором вы можете узнать информацию о преподавателях\n" +
                    Emoji.Watch + "*Время:* Меню, в котором можно узнать все, что связано с врменными периодами\n" +
                    Emoji.Back_Arrow + "*Назад:* В стартовое меню\n" +
                    Emoji.Cross_Mark + "*Закрыть:* Закрывает меню");

            /** Cоздаем клавиатуру */

            ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
            s.setReplyMarkup(replyKeyboardMarkup);

            /** Устанавливаем свойства */

            /** Выборочное открытие клавиатуры (открывается только у челоевка, который ее запросил) */
            replyKeyboardMarkup.setSelective(true);
            /** Подгонка размера клавиатуры по высоте */
            replyKeyboardMarkup.setResizeKeyboard(true);
            /** Закрытие клавиатуры после единократного использования */
            replyKeyboardMarkup.setOneTimeKeyboard(false);

            /** Создаем список строк клавиатуры */
            List<KeyboardRow> keyboard = new ArrayList<>();

            /** Первая строчка клавиатуры */
            KeyboardRow keyboardFirstRow = new KeyboardRow();
            /** Добавляем кнопки в первую строчку клавиатуры */
            keyboardFirstRow.add(new KeyboardButton(Emoji.Page_With_Curl + "Расписание"));
            keyboardFirstRow.add(new KeyboardButton(Emoji.World_Map + "Пара"));

            /** Вторая строчка клавиатуры */
            KeyboardRow keyboardSecondRow = new KeyboardRow();
            /** Добавляем кнопки во вторую строчку клавиатуры */
            keyboardSecondRow.add(new KeyboardButton(Emoji.Man_Student + "Преподаватель"));
            keyboardSecondRow.add(new KeyboardButton(Emoji.Watch + "Время"));

            /** Третья строчка клавиатуры */
            KeyboardRow keyboardThirdRow = new KeyboardRow();
            /** Добавляем кнопки в третью строчку клавиатуры */
            keyboardThirdRow.add(new KeyboardButton(Emoji.Back_Arrow + "Назад"));
            keyboardThirdRow.add(new KeyboardButton(Emoji.Cross_Mark + "Закрыть"));

            /** Добавляем все строчки клавиатуры в список */
            keyboard.add(keyboardFirstRow);
            keyboard.add(keyboardSecondRow);
            keyboard.add(keyboardThirdRow);

            /** Устанваливаем этот список нашей клавиатуре */
            replyKeyboardMarkup.setKeyboard(keyboard);
            try
            {
                execute(s);
                Main._Log.info("Отправлена клавиатура команд\n");
            }
            catch (TelegramApiException e)
            {
                Main._Log.warn("Клавиатура команд не отправлена: \n" + e.toString() + "\n");
            }
        }

        /** Метод отправки клавиатуры команд расписания */
        private void sendScheduleKeyboard(Message msg, Boolean replyMod)
        {
            SendMessage s = new SendMessage();
            s.enableMarkdown(true);
            s.setChatId(msg.getChatId());
            if (replyMod)
                s.setReplyToMessageId(msg.getMessageId());
            s.setText("*Вы находитесь в меню команд расписания*\n" +
                    "*Выберите одну из опций:*\n" +
                    Emoji.Open_Book + "*Сегодня:* Выводит расписание на сегодня\n" +
                    Emoji.Green_Book + "*Завтра:* Выводит расписание на завтра\n" +
                    Emoji.Orange_Book + "*Дата:* Выводит расписание на определенный день\n" +
                    Emoji.Closed_Book + "*Неделя:* Выводит расписание на неделю вперед\n" +
                    Emoji.Paperclip + "*Файл расписания:* Присылает файл расписания\n" +
                    Emoji.Left_Arrow + "*Назад к командам:* В меню команд\n" +
                    Emoji.Cross_Mark + "*Закрыть:* Закрывает меню");

            /** Cоздаем клавиатуру */

            ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
            s.setReplyMarkup(replyKeyboardMarkup);

            /** Устанавливаем свойства */

            /** Выборочное открытие клавиатуры (открывается только у челоевка, который ее запросил) */
            replyKeyboardMarkup.setSelective(true);
            /** Подгонка размера клавиатуры по высоте */
            replyKeyboardMarkup.setResizeKeyboard(true);
            /** Закрытие клавиатуры после единократного использования */
            replyKeyboardMarkup.setOneTimeKeyboard(false);

            /** Создаем список строк клавиатуры */
            List<KeyboardRow> keyboard = new ArrayList<>();

            /** Первая строчка клавиатуры */
            KeyboardRow keyboardFirstRow = new KeyboardRow();
            /** Добавляем кнопки в первую строчку клавиатуры */
            keyboardFirstRow.add(new KeyboardButton(Emoji.Open_Book + "Сегодня"));
            keyboardFirstRow.add(new KeyboardButton(Emoji.Green_Book + "Завтра" ));
            keyboardFirstRow.add(new KeyboardButton(Emoji.Orange_Book + "Дата"));

            /** Вторая строчка клавиатуры */
            KeyboardRow keyboardSecondRow = new KeyboardRow();
            /** Добавляем кнопки во вторую строчку клавиатуры */
            keyboardSecondRow.add(new KeyboardButton(Emoji.Paperclip + "Файл расписания"));
            keyboardSecondRow.add(new KeyboardButton(Emoji.Closed_Book + "Неделя"));

            /** Третья строчка клавиатуры */
            KeyboardRow keyboardThirdRow = new KeyboardRow();
            /** Добавляем кнопки в третью строчку клавиатуры */
            keyboardThirdRow.add(new KeyboardButton(Emoji.Left_Arrow + "Назад к командам"));
            keyboardThirdRow.add(new KeyboardButton(Emoji.Cross_Mark + "Закрыть"));

            /** Добавляем все строчки клавиатуры в список */
            keyboard.add(keyboardFirstRow);
            keyboard.add(keyboardSecondRow);
            keyboard.add(keyboardThirdRow);

            /** Устанваливаем этот список нашей клавиатуре */
            replyKeyboardMarkup.setKeyboard(keyboard);
            try
            {
                execute(s);
                Main._Log.info("Отправлена клавиатура команд расписания\n");
            }
            catch (TelegramApiException e)
            {
                Main._Log.warn("Клавиатура команд расписания не отправлена: \n" + e.toString() + "\n");
            }
        }

        /** Метод отправки клавиатуры команд пары */
        private void sendClassKeyboard(Message msg, Boolean replyMod)
        {
            SendMessage s = new SendMessage();
            s.enableMarkdown(true);
            s.setChatId(msg.getChatId());
            if (replyMod)
                s.setReplyToMessageId(msg.getMessageId());
            s.setText("*Вы находитесь в меню команд пары*\n" +
                    "*Выберите одну из опций:*\n" +
                    Emoji.Pen + "*Текущая:* В какой аудитории текущая пара\n" +
                    Emoji.Pencil + "*Следующая:*  В какой аудитории следующая пара\n" +
                    Emoji.Fountain_Pen + "*Определенная:*  В какой аудитории определенная пара\n" +
                    Emoji.Crayon + "*Аудитория:* Где расположена аудитория\n" +
                    Emoji.Left_Arrow + "*Назад к командам:* В меню команд\n" +
                    Emoji.Cross_Mark + "*Закрыть:* Закрывает меню");

            /** Cоздаем клавиатуру */

            ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
            s.setReplyMarkup(replyKeyboardMarkup);

            /** Устанавливаем свойства */

            /** Выборочное открытие клавиатуры (открывается только у челоевка, который ее запросил) */
            replyKeyboardMarkup.setSelective(true);
            /** Подгонка размера клавиатуры по высоте */
            replyKeyboardMarkup.setResizeKeyboard(true);
            /** Закрытие клавиатуры после единократного использования */
            replyKeyboardMarkup.setOneTimeKeyboard(false);

            /** Создаем список строк клавиатуры */
            List<KeyboardRow> keyboard = new ArrayList<>();

            /** Первая строчка клавиатуры */
            KeyboardRow keyboardFirstRow = new KeyboardRow();
            /** Добавляем кнопки в первую строчку клавиатуры */
            keyboardFirstRow.add(new KeyboardButton(Emoji.Pen + "Текущая"));
            keyboardFirstRow.add(new KeyboardButton(Emoji.Pencil + "Следующая" ));

            /** Вторая строчка клавиатуры */
            KeyboardRow keyboardSecondRow = new KeyboardRow();
            /** Добавляем кнопки во вторую строчку клавиатуры */
            keyboardSecondRow.add(new KeyboardButton( Emoji.Fountain_Pen + "Определенная"));
            keyboardSecondRow.add(new KeyboardButton(Emoji.Crayon + "Аудитория"));

            /** Третья строчка клавиатуры */
            KeyboardRow keyboardThirdRow = new KeyboardRow();
            /** Добавляем кнопки в третью строчку клавиатуры */
            keyboardThirdRow.add(new KeyboardButton(Emoji.Left_Arrow + "Назад к командам"));
            keyboardThirdRow.add(new KeyboardButton(Emoji.Cross_Mark + "Закрыть"));

            /** Добавляем все строчки клавиатуры в список */
            keyboard.add(keyboardFirstRow);
            keyboard.add(keyboardSecondRow);
            keyboard.add(keyboardThirdRow);

            /** Устанваливаем этот список нашей клавиатуре */
            replyKeyboardMarkup.setKeyboard(keyboard);
            try
            {
                execute(s);
                Main._Log.info("Отправлена клавиатура команд пары\n");
            }
            catch (TelegramApiException e)
            {
                Main._Log.warn("Клавиатура команд пары не отправлена: \n" + e.toString() + "\n");
            }
        }

        /** Метод отправки клавиатуры команд преподавателя */
        private void sendTeacherKeyboard(Message msg, Boolean replyMod)
        {
            SendMessage s = new SendMessage();
            s.enableMarkdown(true);
            s.setChatId(msg.getChatId());
            if (replyMod)
                s.setReplyToMessageId(msg.getMessageId());
            s.setText("*Вы находитесь в меню команд преподавателя*\n" +
                    "*Выберите одну из опций:*\n" +
                    Emoji.Man + "*Имя:* Полное имя преподавателя по фамилии\n" +
                    Emoji.Man_Pouting + "*Дисциплина:* Полное имя преподавателя по названию дисциплины\n" +
                    Emoji.Memo + "*Контакты:* Телефон и почта преподавателя (если их внесли в базу)\n" +
                    Emoji.Left_Arrow + "*Назад к командам:* В меню команд\n" +
                    Emoji.Cross_Mark + "*Закрыть:* Закрывает меню");

            /** Cоздаем клавиатуру */

            ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
            s.setReplyMarkup(replyKeyboardMarkup);

            /** Устанавливаем свойства */

            /** Выборочное открытие клавиатуры (открывается только у челоевка, который ее запросил) */
            replyKeyboardMarkup.setSelective(true);
            /** Подгонка размера клавиатуры по высоте */
            replyKeyboardMarkup.setResizeKeyboard(true);
            /** Закрытие клавиатуры после единократного использования */
            replyKeyboardMarkup.setOneTimeKeyboard(false);

            /** Создаем список строк клавиатуры */
            List<KeyboardRow> keyboard = new ArrayList<>();

            /** Первая строчка клавиатуры */
            KeyboardRow keyboardFirstRow = new KeyboardRow();
            /** Добавляем кнопки в первую строчку клавиатуры */
            keyboardFirstRow.add(new KeyboardButton(Emoji.Man + "Имя"));
            keyboardFirstRow.add(new KeyboardButton(Emoji.Man_Pouting + "Дисциплина"));
            keyboardFirstRow.add(new KeyboardButton(Emoji.Memo + "Контакты"));

            /** Вторая строчка клавиатуры */
            KeyboardRow keyboardSecondRow = new KeyboardRow();
            /** Добавляем кнопки во вторую строчку клавиатуры */
            keyboardSecondRow.add(new KeyboardButton(Emoji.Left_Arrow + "Назад к командам"));
            keyboardSecondRow.add(new KeyboardButton(Emoji.Cross_Mark + "Закрыть"));

            /** Добавляем все строчки клавиатуры в список */
            keyboard.add(keyboardFirstRow);
            keyboard.add(keyboardSecondRow);

            /** Устанваливаем этот список нашей клавиатуре */
            replyKeyboardMarkup.setKeyboard(keyboard);
            try
            {
                execute(s);
                Main._Log.info("Отправлена клавиатура команд преподавателя\n");
            }
            catch (TelegramApiException e)
            {
                Main._Log.warn("Клавиатура команд преподавателя не отправлена: \n" + e.toString() + "\n");
            }
        }

        /** Метод отправки клавиатуры команд времени */
        private void sendTimeKeyboard(Message msg, Boolean replyMod)
        {
            SendMessage s = new SendMessage();
            s.enableMarkdown(true);
            s.setChatId(msg.getChatId());
            if (replyMod)
                s.setReplyToMessageId(msg.getMessageId());
            s.setText("*Вы находитесь в меню команд времени*\n" +
                    "*Выберите одну из опций:*\n" +
                    Emoji.Twelve_O_clock + "*Сессия:* Сколько времени осталось до сессии\n" +
                    Emoji.Three_O_clock + "*Неделя:* Какая сейчас идет неделя\n" +
                    Emoji.Six_O_clock + "*Пары:* Временное расписание пар\n" +
                    Emoji.Left_Arrow + "*Назад к командам:* В меню команд\n" +
                    Emoji.Cross_Mark + "*Закрыть:* Закрывает меню");

            /** Cоздаем клавиатуру */

            ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
            s.setReplyMarkup(replyKeyboardMarkup);

            /** Устанавливаем свойства */

            /** Выборочное открытие клавиатуры (открывается только у челоевка, который ее запросил) */
            replyKeyboardMarkup.setSelective(true);
            /** Подгонка размера клавиатуры по высоте */
            replyKeyboardMarkup.setResizeKeyboard(true);
            /** Закрытие клавиатуры после единократного использования */
            replyKeyboardMarkup.setOneTimeKeyboard(false);

            /** Создаем список строк клавиатуры */
            List<KeyboardRow> keyboard = new ArrayList<>();

            /** Первая строчка клавиатуры */
            KeyboardRow keyboardFirstRow = new KeyboardRow();
            /** Добавляем кнопки в первую строчку клавиатуры */
            keyboardFirstRow.add(new KeyboardButton(Emoji.Twelve_O_clock + "Сессия"));
            keyboardFirstRow.add(new KeyboardButton( Emoji.Three_O_clock + "Неделя"));
            keyboardFirstRow.add(new KeyboardButton(Emoji.Six_O_clock + "Пары"));

            /** Вторая строчка клавиатуры */
            KeyboardRow keyboardSecondRow = new KeyboardRow();
            /** Добавляем кнопки во вторую строчку клавиатуры */
            keyboardSecondRow.add(new KeyboardButton(Emoji.Left_Arrow + "Назад к командам"));
            keyboardSecondRow.add(new KeyboardButton(Emoji.Cross_Mark + "Закрыть"));

            /** Добавляем все строчки клавиатуры в список */
            keyboard.add(keyboardFirstRow);
            keyboard.add(keyboardSecondRow);

            /** Устанваливаем этот список нашей клавиатуре*/
            replyKeyboardMarkup.setKeyboard(keyboard);
            try
            {
                execute(s);
                Main._Log.info("Отправлена клавиатура команд времени\n");
            }
            catch (TelegramApiException e)
            {
                Main._Log.warn("Клавиатура команд времени не отправлена: \n" + e.toString() + "\n");
            }
        }

        /** Метод отправки клавиатуры админки */
        private void sendAdminKeyboard(Message msg, Boolean replyMod)
        {
            SendMessage s = new SendMessage();
            s.enableMarkdown(true);
            s.setChatId(msg.getChatId());
            if (replyMod)
                s.setReplyToMessageId(msg.getMessageId());
            s.setText("*Вы находитесь в меню администратора*\n" +
                    "*Выберите одну из опций:*\n" +
                    Emoji.Keyboard + "*Лог:* Выводит файл лога\n" +
                    Emoji.Keyboard + "*Запрос:* Позволяет ввести необходимый запрос\n" +
                    Emoji.Left_Arrow + "*Выход  из админки:* выход из меню администратора и переход к основному меню\n");

            /** Cоздаем клавиатуру */

            ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
            s.setReplyMarkup(replyKeyboardMarkup);

            /** Устанавливаем свойства */

            /** Выборочное открытие клавиатуры (открывается только у челоевка, который ее запросил) */
            replyKeyboardMarkup.setSelective(true);
            /** Подгонка размера клавиатуры по высоте */
            replyKeyboardMarkup.setResizeKeyboard(true);
            /** Закрытие клавиатуры после единократного использования */
            replyKeyboardMarkup.setOneTimeKeyboard(false);

            /** Создаем список строк клавиатуры */
            List<KeyboardRow> keyboard = new ArrayList<>();

            /** Первая строчка клавиатуры */
            KeyboardRow keyboardFirstRow = new KeyboardRow();
            /** Добавляем кнопки в первую строчку клавиатуры */
            keyboardFirstRow.add(new KeyboardButton(Emoji.Keyboard + "Лог"));
            keyboardFirstRow.add(new KeyboardButton( Emoji.Keyboard + "Запрос"));

            /** Вторая строчка клавиатуры */
            KeyboardRow keyboardSecondRow = new KeyboardRow();
            /** Добавляем кнопки во вторую строчку клавиатуры */
            keyboardSecondRow.add(new KeyboardButton( Emoji.Left_Arrow + "Выход  из админки"));

            /** Добавляем все строчки клавиатуры в список */
            keyboard.add(keyboardFirstRow);
            keyboard.add(keyboardSecondRow);

            /** Устанваливаем этот список нашей клавиатуре*/
            replyKeyboardMarkup.setKeyboard(keyboard);
            try
            {
                execute(s);
                Main._Log.info("Отправлена клавиатура администратора\n");
            }
            catch (TelegramApiException e)
            {
                Main._Log.warn("Клавиатура администратора не отправлена: \n" + e.toString() + "\n");
            }
        }

        private void sendHideKeyboard(Message msg, Boolean replyMod)
        {
            SendMessage s = new SendMessage();
            s.setChatId(msg.getChatId());
            if (replyMod)
                s.setReplyToMessageId(msg.getMessageId());
            s.enableMarkdown(true);
            s.setText("*Меню закрыто!*\n" +
                    "Для повторного вызова меню введите */bot*");

            ReplyKeyboardRemove replyKeyboardRemove = new ReplyKeyboardRemove();
            replyKeyboardRemove.setSelective(true);
            s.setReplyMarkup(replyKeyboardRemove);

            try
            {
                execute(s);
                Main._Log.info("Отправлено скрытие клавиатуры\n");
            }
            catch (TelegramApiException e)
            {
                Main._Log.warn("Клавиатура не скрыта: \n" + e.toString() + "\n");
            }
        }

        private String firstUpperCase(String word)
        {
            return word.substring(0, 1).toUpperCase() + word.substring(1);
        }

        private static boolean checkDate(String date)
        {
            Pattern p = Pattern.compile("\\d\\d\\.\\d\\d\\.\\d\\d\\d\\d");
            Matcher m = p.matcher(date);
            return  m.matches();
        }

    }

