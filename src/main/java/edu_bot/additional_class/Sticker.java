package edu_bot.additional_class;

public enum Sticker
{
        Cat_Sorry("CAADAgADVwAD6VUFGJcpm0jgsOb1Ag");

    String sticker;

    Sticker(String sticker)
    {
        this.sticker = sticker;
    }

    @Override
    public String toString()
    {
        String sb = "";
        sb = sb + this.sticker;
        return sb;
    }
}
